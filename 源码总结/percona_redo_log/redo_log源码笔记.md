# Redo Log 模块源码笔记

- [Redo Log 模块源码笔记](#redo-log-模块源码笔记)
  - [源码文件](#源码文件)
    - [log0log.h](#log0logh)
      - [redo log header](#redo-log-header)
      - [服务器变量（默认、最小和最大值）](#服务器变量默认-最小和最大值)
      - [模块变量](#模块变量)
      - [内联函数声明](#内联函数声明)
      - [log_buffer函数](#log_buffer函数)
    - [os/os0event.cc](#osos0eventcc)
    - [ut0link_buf.h](#ut0link_bufh)
      - [Link_buf](#link_buf)
        - [简要](#简要)
        - [成员](#成员)
        - [函数列表](#函数列表)
    - [log0types.h](#log0typesh)
    - [log0log.ic](#log0logic)
    - [log0log.cc](#log0logcc)
      - [注释](#注释)
        - [Innodb redo log](#innodb-redo-log)
        - [Redo Log的架构](#redo-log的架构)
        - [Redo Log的一般规则](#redo-log的一般规则)
        - [LSN值的术语表](#lsn值的术语表)
          - [redo log的头部](#redo-log的头部)
          - [redo log的尾部](#redo-log的尾部)
      - [重要数据结构](#重要数据结构)
      - [函数清单](#函数清单)

## 源码文件
> 为了实现统一的管理，与redo日志不同，undo日志在Buffer Pool中有对应的数据页，与普通的数据页一起管理，依据LRU规则也会被淘汰出内存，后续再从磁盘读取。与普通的数据页一样，对undo页的修改，也需要先写redo日志。

### log0log.h
> 头文件声明，包含多个源码文件中的函数声明（对外接口）

#### redo log header
+ checkpoint1
+ encryption (加密)
+ checkpoint2
+ log file header的大小

#### 服务器变量（默认、最小和最大值）

#### 模块变量
+ extern log_t *log_sys;  /** Redo log system (singleton). */

+ 日志校验和函数指针

#### 内联函数声明
> 定义在log0log.ic文件中

#### log_buffer函数
> 定义在log0buf.cc
+ log buffer 函数
+ remaining 函数
+ 加密函数
+ log file header 相关函数
+ log buffer SX锁函数
+ log buffer resize 函数
+ log system 函数 ：init、start、close
+ log 后台线程操作函数
+ log 多种互斥量

### os/os0event.cc
> 操作系统条件变量的封装接口
结构体 os_event，每个os_event可以有一个字符串名称标识
```
os_event_set() => os_event->is_set=true => 线程继续运行

os_event_reset() => os_event->is_set=false => 线程等待

```
使用reset_sig_count防止多线程reset引起的无限等待，使用timeout（微秒）限制等待时延。

### ut0link_buf.h
> percona-server/storage/innobase/include/ut0link_buf.h
>
> Link buffer : 并发数据结构（2017-8-30创建）
> 1. 并发的添加链接
> 2. 单线程追踪link创建的链接路径
> 3. 限制窗口的空洞的大小（缺少link）

#### Link_buf
> Link_buf模板类，Position类型是uint64_t

##### 简要
并发数据结构，用于追踪逻辑上可能乱序的并发执行操作。
该数据结构告知并发操作的完成和追踪所有的操作完成的总顺序（无空洞）。
它也可以限制可能出现空洞的最后时期。这些空洞是未完成的并发操作，这些操作之前的操作已经完成。
线程可能会并发回报完成的操作（lock-free）。
线程可能会询问全序中当前已知的最大的位置，直到所有的操作已经完成（lock-free）。
单个线程可能追踪可以回报的已经完成的操作和更新全序中的最大位置，直到所有的操作已经完成。
该数据结构用于log0buf.cc

##### 成员
+ m_capacity
+ m_links
+ m_tail

##### 函数列表
+ 构造函数、析构函数、复制构造函数、赋值符号重载
+ add_link：在from的位置存储to-from
+ next_position：从position位置加上数组中存的值，计算出add_link时的to存入next引用，返回数组位置存储的值是否为0
+ claim_position：数组中position位置设置为0
+ advance_tail_until：从尾部开始，不断next_position直到满足停止条件或无可以next的，访问过的数组位置归0
+ advance_tail：用lambda表达式定义了停止条件，然后调用advance_tail_until
+ capacity：返回capacity大小
+ tail：返回m_tail
+ has_space：返回请求的位置是否小于tail() + m_capacity
+ slot_index : 对数组大小取模
+ 两个debug用的验证函数

### log0types.h
> redo log类型
+ lsn、sn、checkpoint_no等相关的基本类型的声明
+ log_header_format_t 控制redo log文件的头部信息格式
+ struct alignas(INNOBASE_CACHE_LINE_SIZE) log_t
    Redo log：单个描述redo日志系统的数据结构。未来可能会分割成多个。

### log0log.ic
> 一些内联函数的定义

### log0log.cc

#### 注释
> redo log 系统 ：提供了未刷盘的数据页上的修改的持久化

该文件包含：
1. 分配和销毁redo log数据结构
2. 初始化和停止redo log
3. 启动和停止日志后台线程
4. 运行时更新服务端变量
5. 扩展redo log buffer的大小
6. 锁住redo log的位置（replication）

+ 负责写redo log的代码在log0buf.cc、log0write.cc和log0log.ic文件中。
+ log writer、fulsher、write notifier、fulshnotifier和closer线程实现于log0write.cc文件中。
+ 检查点与检查点线程的实现在log0chkp.cc文件中。

##### Innodb redo log
+ 当clean shutdown后，redo log在逻辑上是空的，这意味着在检查点lsn后应该没有日志记录可以应用。尽管日志文件里还包含了一些旧数据，在恢复过程中不会使用。
+ 数据页的每一部分修改都必须通过mtr完成，在mtr_commit()函数中写所有的日志记录。
+ 一般来说这些变更使用mlog_write_ulint()或相似的函数实施。在一些页级别的操作，仅有c函数的code number和参数写到redo log中，以此减少redo log的大小。（1）不应该增加这些函数的参数；（2）不应该增加与旧版本不同的功能和依赖于正在修改的页之外的数据的功能。
+ 因此，所有函数必须实现独立的页面转换。并且，若没有必需的理由，不应该改变日志的语义或格式。
+ 单个mtr可以包含对多个页的变更。当发生crash时，给定的mtr的变更要不全部恢复要不没有一个会被恢复。
+ 在mtr的生命周期里，日志收集在mtr内部的一个buffer里。当mtr提交时，所有的日志记录都会写入到一个日志组的log buffer。
+ 步骤：
    1. 计算日志记录的数据类型的数量。
    2. 保留日志记录需要的空间。lsn值的范围赋予给一组日志记录。
    3. 日志记录写到log buffer预留的空间
    4. 修改的页标记为脏页并移动到flush链表。所有的脏页被标记为指定范围的lsn。
    5. 关闭预留的空间。

+ 后台线程负责将log buffer中新的变更写到log文件中。用户线程需要日志的持久性，所以必须等待日志刷盘到需求的点。

+ 当恢复时，只有完整的日志组会被恢复和应用。

+ 写入到redo log中的连续字节由lsn枚举，lsn按照字节数递增。

+ redo log中数据结构是连续的512字节的块(_OS_FILE_LOG_BLOCK_SIZE_)，每个块包含12个字节的头部(_LOG_BLOCK_HDR_SIZE_)和一个4字节的尾部(_LOG_BLOCK_TRL_SIZE_)。这些额外的字节也由lsn枚举。
+ 数据字节是指日志块中除了头部与尾部的部分，数据字节的枚举值称为sn。

+ 当一个用户事务提交时，额外的mtr会被提交（与undo log有关）。然后，用户线程等待redo log刷盘。

+ 当一个脏页被刷盘时，一个线程负责刷盘。首先，需要等待redo log刷盘到页的最新的修改；然后，页可能会被修改。
+
+ 当发生crash时，我们可能结束于页的最新版本而没有更早版本的页，其它的页可能未刷盘，因此需要恢复到应有的版本。所以需要应用：
   + 相同日志组的页修改
   + 更早日志组的页修改

##### Redo Log的架构
 redo log包含以下几个数据层：
  1. 日志文件（典型4-32GB）：驻留在磁盘上的物理redo日志文件。
  2. Log buffer（默认64MB）：写到rodo日志文件的日志组，包含日志块的头部、尾部，计算校验和，日志记录组的边界。
  3. Log recent written buffer（例如4MB）：追踪最近写到log buffer的日志记录。允许并发写入log buffer，追踪已经完成写入的lsn。
  4. Log recent closed buffer（例如4MB）：追踪最近写入的对应脏页已经添加到flush链表的日志记录。制作检查点时不能涉及大于未加入flush链表的脏页的最旧修改的lsn，因为用户线程已经被调度。
  5. Log write ahead buffer（例如4KB）：用于预写更多字节到redo日志文件，避免read-on-write问题。该buffer也用于需要写不完整的log block时，当并发接收另一个用户线程的数据时，可以先将不完整的block复制到该buffer中。

##### Redo Log的一般规则
 + 用户线程仅将redo数据写到log buffer
 + 用户线程并发写日志到log buffer，不与其它用户线程同步
 + log recent written buffer用于追踪并发写
 + 后台日志线程负责写和刷盘log buffer
 + 用户线程不会接触到日志文件，后台日志线程仅被允许接触到日志文件
 + 当用户线程需要已刷盘的redo log时，用户线程等待后台日志线程
 + 如果用户对等待redo log刷盘有兴趣，每个日志块有一个对应的事件
 + 用户可以获知已经写入或刷盘的日志点
 + 如果log buffer没有空间，用户线程需要等待
 + 用户线程以松散的顺序将脏页添加到flush链表
 + 用户线程预留lsn范围的顺序、写日志到log buffer的顺序和添加脏页到flush链表的顺序可以是三种完全不同的顺序
 + 用户线程不会写检查点（不允许接触日志文件）
 + 检查点由后台线程时不时的自动写入
 + 用户线程可以请求一次强制写检查点和等待
 + 如果日志文件里没有空间，用户线程需要等待
 + log buffer可以由用户动态resize

##### LSN值的术语表

###### redo log的头部
+ subsect_redo_log_write_lsn
    到此lsn为止，我们将所有日志写到了日志文件中。它是未写盘的log buffer的起点。小于该lsn的字节不被需要，可能会被循环写入的日志复写。值由log writer线程更新。
+ subsect_redo_log_buf_ready_for_write_lsn
    到此lsn为止，所有log buffer的并发写已经完成。很显然，` log.buf_ready_for_write_lsn >= log.write_lsn`。值由log writer线程更新。
    + subsect_redo_log_flushed_to_disk_lsn log.flushed_to_disk_lsn
        到此lsn为止，我们已经写入并刷盘日志到日志文件。显然有`log.flushed_to_disk_lsn <= log.write_lsn`，该值由log flusher线程更新。
+ subsect_redo_log_sn
    于当前lsn相关，最大的sn值（仅以数据字节枚举），显然`log.sn >= log_translate_lsn_to_sn(log.buf_ready_for_write_lsn)`，值由用户线程预留空间时更新。

###### redo log的尾部
+ subsect_redo_log_buf_dirty_pages_added_up_to_lsn
    到此lsn为止，用户线程已经将所有的脏页添加到flush链表。
    到此lsn为止的日志记录可以被删除。
    注意，我们不会物理上删除redo log，但是仍然可以逻辑上删除，在给定的lsn上做检查点。
    保持`log.buf_dirty_pages_added_up_to_lsn <= log.buf_ready_for_write_lsn`
    值由log closer线程更新。
+ subsect_redo_log_available_for_checkpoint_lsn
    到此lsn为止，所有的脏页都刷到磁盘上。这个值在上述这些值中是最大的。用于为下一个检查点更新可用的lsn。
    值由log checkpointer线程更新。
+ subsect_redo_log_last_checkpoint_lsn
    到此lsn为止，所有的脏页已经刷到磁盘上并且lsn值已经被刷盘到第一个日志文件的头部。(_ib_logfile0_)
    这个lsn值指向恢复开始的地方。相似的lsn值的数据类型不需要，并且可能被复写（日志文件是循环）。可以认为它们逻辑上被删除了。
    值由log checkpointer线程更新。
    `log.last_checkpoint_lsn<= log.available_for_checkpoint_lsn<= log.buf_dirty_pages_added_up_to_lsn`

更多redo log的细节参考：
- @subpage PAGE_INNODB_REDO_LOG_BUF
- @subpage PAGE_INNODB_REDO_LOG_THREADS
- @subpage PAGE_INNODB_REDO_LOG_FORMAT

#### 重要数据结构
+ log_t *log_sys（单例模式）

#### 函数清单

+ log_sys_init 初始化日志系统。注意在此函数调用后日志系统没有准备好服务用户写。正确的调用顺序是：
```
        - log_sys_init(),
        - log_start(),
        - log_start_background_threads()
```
+ log_start  借助给定的检查点lsn与当前lsn初始化日志系统。在调用该函数之前当前lsn对应的日志buffer中的块必须已经被初始化。初始化了许多前一个函数分配了空间但未设定值的成员变量。
+ log_sys_close 释放日志系统数据结构，释放所有相关的内存

+ log_writer_thread_active_validate 线程活跃验证函数
+ log_closer_thread_active_validate
+ log_background_write_threads_active_validate
+ log_background_threads_active_validate
+ log_background_threads_inactive_validate

+  log_start_background_threads 开始所有后台线程。该函数仅能在线程不活跃时启动，不能被并发调用，不能在只读模式下调用。流程：（1）验证后台线程是否处于不活跃状态；（2）创建后台线程；（3）启动线程。
+  log_stop_background_threads 与上一个函数相反，停止所有日志后台线程。当持有x-lock时，无法停止线程。流程：唤醒线程，当前线程休眠，直到唤醒的线程停止。
+  log_stop_background_threads_nowait 标记标志，告知日志线程停止并唤醒，并不等待它们停止。
+  log_wake_threads 唤醒所有的日志线程。
+  log_print 状态打印函数。
+  log_buffer_resize_low 改变log buffer的size。这是一个非线程安全的函数，仅能在没有并发写的场景下调用。用于log_buffer_reserve()函数请求保留的size大于log buffer的size。简要：日志buffer将数据拷贝到临时buffer，然后销毁原本的日志buffer，分配新的日志buffer，将临时buffer的内容拷贝到新的日志buffer。
+  log_buffer_resize 改变日志buffer的size。这是一个线程安全的版本，用于`SET GLOBAL innodb_log_buffer_size = X`。上一个函数添加了锁与互斥量的包装。
+  log_write_ahead_resize 调整预写buffer的大小。同样通过临时buffer转移数据。
+  log_calc_buf_size 计算日志buffer的恰当大小并更新相关的域。计算是基于srv_log_buffer_size的，是一个二的幂。流程：将log.buf_size设置为srv_log_buffer_size，并修改sn与调用log_update_limits()。
+  log_allocate_buffer 计算恰当的日志buffer大小并分配日志buffer。根据系统变量的值分配buffer。
+  log_deallocate_buffer 释放日志buffer的空间。
+  log_allocate_write_ahead_buffer
+  log_deallocate_write_ahead_buffer
+  log_allocate_checkpoint_buffer
+  log_deallocate_checkpoint_buffer
+  log_allocate_flush_events 分配srv_log_flush_events个flush events。
+  log_deallocate_flush_events 释放fulsh_events
+  log_allocate_write_events
+  log_deallocate_write_events
+  log_allocate_recent_written 分配的recent_written是一个Link_buf。
+  log_deallocate_recent_written
+  log_allocate_recent_closed 分配的recent_close也是一个Link_buf。
+  log_deallocate_recent_closed
+  log_allocate_file_header_buffers 分配每个日志文件的文件头buffer。
+  log_deallocate_file_header_buffers
+  日志位置锁（用于replication）：
    +  log_position_lock 锁住redo log，直到解锁为止，当前lsn与检查点lsn都不会变化。
    +  log_position_unlock  解除redo log锁
    +  log_position_collect_lsn_info 在锁住的redo log中获取当前lsn与检查点lsn。
    +  log_pad_current_log_block 以下三个函数是scrub线程的内部逻辑，内部实现逻辑就是用虚拟的日志记录填充满日志块。
    +  log_scrub
    +  log_scrub_thread