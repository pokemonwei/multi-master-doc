
# Redo Log 物理日志回放

- [Redo Log 物理日志回放](#redo-log-物理日志回放)
  - [InooDB 崩溃恢复](#inoodb-崩溃恢复)
    - [前提](#前提)
    - [崩溃恢复的流程](#崩溃恢复的流程)
      - [一、恢复truncate流程](#一-恢复truncate流程)
      - [二、定位扫描起点](#二-定位扫描起点)
      - [三、三次扫描，解析日志](#三-三次扫描解析日志)
      - [四、应用redo日志](#四-应用redo日志)
      - [五、初始化事务子系统](#五-初始化事务子系统)

## InooDB 崩溃恢复

### 前提

启动时MySQL会检查检查点的LSN判断是否需要执行崩溃恢复，如果需要则初始化崩溃恢复系统recv_sys，准备执行崩溃恢复。

### 崩溃恢复的流程

#### 一、恢复truncate流程

第一阶段是独立于redo log的，通过文件日志恢复未完成的truncate操作，这一操作既包含用户表也包含undo表。

#### 二、定位扫描起点

> 对于正常shutdown的场景，一次checkpoint完成后是不记录MLOG_CHECKPOINT日志的，如果扫描过程中没有找到对应的日志，那就认为上次是正常shutdown的，不用考虑崩溃恢复了。

1. 为每个buffer pool实例创建红黑树，加速page插入到flush list；（由于应用日志恢复page时按照的是哈希表中的顺序，flush list中的顺序是按照LSN排序的）
2. 读取存储在第一个redo log文件头部的检查点LSN，并定位到redo日志文件对应位置，以此为扫描的起点。

#### 三、三次扫描，解析日志

> 调用三次`recv_group_scan_log_recs`扫描redo log文件

1. 第一次扫描，找到MLOG_CHECKPOINT日志。MLOG_CHECKPOINT日志设计的目的是简化 InnoDB 崩溃恢复的逻辑。该日志记录了检查点LSN，要找到与redo log文件头记录的检查点LSN相同的日志记录。

2. 第二次扫描，存储日志对象。日志解析后的对象类型为`recv_t`，包含日志类型、长度、数据、开始和结束LSN。日志对象的存储使用hash结构，根据 space id 和 page no 计算hash值，相同页上的变更作为链表节点链在一起。哈希表结构如下图：

   ![recv_hash](images/recv_hash.png)

   扫描的过程中，会基于MLOG_FILE_NAME 和MLOG_FILE_DELETE 这样的redo日志记录来构建`recv_spaces`，存储space id到文件信息的映射（`fil_name_parse` –> `fil_name_process`），这些文件可能需要进行崩溃恢复。

   默认情况下，Redo log以一批64KB（RECV_SCAN_SIZE）为单位读入到`log_sys->buf`中，然后调用函数`recv_scan_log_recs`处理日志块。这里会判断到日志块的有效性：是否是完整写入的、日志块checksum是否正确，并检查一些标志位。

   对于合法的日志，会拷贝到缓冲区`recv_sys->buf`中，调用函数`recv_parse_log_recs`解析日志记录。

   日志的操作类型有50多种，具体的分类处理方式可以参考`recv_parse_or_apply_log_rec_body`函数。

   第二次扫描只会应用MLOG_FILE_*类型的日志，记录到`recv_spaces`中，对于其他类型的日志在解析后存储到哈希对象里。然后调用函数`recv_init_crash_recovery_spaces`对涉及的表空间进行初始化处理。

3. 第三次扫描，如果第二次扫描hash表空间不足，无法全部存储到hash表中，则发起第三次扫描，清空hash，重新从checkpoint点开始扫描，解析一批，应用一批。

#### 四、应用redo日志

> redo前滚

根据之前搜集到`recv_sys->addr_hash`中的日志记录，依次将page读入内存，并对每个page进行崩溃恢复操作（`recv_recover_page_func`）

#### 五、初始化事务子系统

> undo回滚

执行完redo前滚数据库，数据库的所有数据页已经处于一致的状态，undo回滚数据库就可以安全的执行了。数据库崩溃的时候可能有一些没有提交的事务或者已经提交的事务，这个时候就需要决定是否提交。主要分为三步，首先是扫描undo日志，重新建立起undo日志链表，接着是，依据上一步建立起的链表，重建崩溃前的事务，即恢复当时事务的状态。最后，就是依据事务的不同状态，进行回滚或者提交。







