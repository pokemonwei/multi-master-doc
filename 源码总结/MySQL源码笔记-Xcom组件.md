# MySQL源码笔记-Xcom组件

> XCOM是MySQL Group Replication插件中的交流模块，基于Paxos自研开发。

## libmysqlgcs库
> 封装XCOM组件的交流库
> 源码位置：percona-server/plugin/group_replication/libmysqlgcs
> 

### 通用接口
#### include/mysql/gcs/gcs_interface.h
##### 描述

- 任意封装需要实现的接口。
- 为了透明的实例化，应该使用工厂模式调用。
- 所有的接口都是面向组的，这意味着所有方法都可以被服务于同一个组的子接口调用。
- 两个主要功能是：
  - 封装启动与结束
  - 允许访问控制、交流与统计接口。
- 典型调用方法：
```cpp
    Gcs_interface *group_if= new My_GCS_Gcs_interface();

    Gcs_interface_parameters params;
    params.add_parameter("name1", "value2");
    if (!group_if->is_initialized())
    {
      group_if->initialize(params);
    }

    // Inject a logger if wanted
    Logger_interface *logger= new My_GCS_Logger_interface();
    group_if->set_logger(logger);

    Gcs_group_identifier *group_id= new Gcs_group_identifier("my_group");

    Gcs_control_interface *ctrl_if= group_if->get_control_session(group_id);

    ctrl_if->join(); // notice here that group id is not used anymore...

    // Do some operations here, retrieve other interfaces...

    ctrl_if->leave();

    group_if->finalize();
```
##### 内容

1. Gcs_interface类
  1. 启动与初始化方法
  1. 动态重配置方法
  1. 关闭方法
  1. 调用控制接口（参数：组标识符）
  1. 调用交流接口
  1. 调用统计接口
  1. 调用管理接口
  1. 设置logger接口
2. 可用的接口实例的枚举量
2. Gcs_interface_factory类（方法统一有两种重载：参数为接口实例的枚举或字符串）
  1. 获取接口实例的指针
  1. 清理Gcs_interface的单例实例
  1. 清理Gcs_interface单例中的线程本地的交流资源（当XCOM后台提供openssl的ssl功能时使用）
#### include/mysql/gcs/gcs_communication_interface.h
##### 描述
该接口代表了所有的交流功能。与其它接口相似，该接口面向组——每个组由一个实例。通过调用函数Gcs_interface::get_communication_session返回。
主要包含了两个部分：

- 消息发送 ： 广播消息到组中，所有的消息数据和其它信息被打包成Gcs_message对象。
- 消息接收 ： 通过Gcs_communication_event_listener class的回调函数实现异步的消息接收。

典型的使用方法：
```cpp
 class my_Gcs_communication_event_listener: Gcs_communication_event_listener
  {
    void on_message_received(const Gcs_message &message)
    {
      //Do something when message arrives...
    }
  }

  //meanwhile in your client code...
  Gcs_communication_interface *gcs_comm_interface_instance; // obtained
                                                            // previously

  Gcs_communication_event_listener listener_instance;

  int ref_handler=
    gcs_comm_interface_instance->add_event_listener(listener_instance);

  Gcs_message *msg= new Gcs_message(<message_params>);

  gcs_comm_interface_instance->send_message(msg);

  //Normal program flow...

  //In the end...
  gcs_comm_interface_instance->remove_event_listener(ref_handler);
```


#### include/mysql/gcs/gcs_communication_event_listener.h
##### 描述
Gcs_communication_event_listener接口由想要接受消息的用户实现。
仅包含一个函数on_message_received()
#### include/mysql/gcs/gcs_control_interface.h
##### 描述
该接口代表了所有控制相关功能。面向组，每个组存在一个实例，通过Gcs_interface::get_control_session方法返回。
包含的方法分为三类：

- 成员关系视图
- 控制事件
- 一般的数据交换



成员关系视图的包含的操作可以分为：

- 属于一个组： join()和leave()
- 状态信息：belongs_to_group() 和 get_current_view() 应该返回活跃的Gcs_view

 
由于接口的异步特性，无论在本地成员还是远程成员，join()与leave()操作的结果需要通过事件告知。这些事件由定义在Gcs_control_event_listener的回调函数传递。这应该由希望接收这些通知的接口的客户端自行实现。


一般数据交换是指在允许在成员之间交换任意数据。数据通过回调函数get_exchangeable_data()获取，此回调函数每次在视图变更时被调用。它陈述了无论何时VC（View Change）发生时希望提供的数据。
幕后必须会发生的事——当从底层gcs接收一个VC时，实例应该开始一次一个或所有成员（依赖于算法）之间的信息交换，发送可交换的数据到正在join的节点。
这些数据在Gcs_control_event_listener::on_view_changed函数被调用时传递，该函数在一个时间点内分发所有交换的数据。
典型的使用方法如下：
```cpp
  class my_Gcs_control_event_listener: Gcs_control_event_listener
  {
    void on_view_changed(const Gcs_view *new_view,
                         const Exchanged_data &exchanged_data)
    {
      // D something when view arrives...
      // It will also deliver all data that nodes decided to offer at join()
      // time
    }

    Gcs_message_data &get_exchangeable_data()
    {
      // Return whatever data we want to provide to a joining node
    }
  }

  // Meanwhile in your client code...
  Gcs_control_interface *gcs_control_interface_instance; // obtained
                                                         // previously

  Gcs_control_event_listener *listener_instance=
    new my_Gcs_control_event_listener();

  int ref_handler=
    gcs_control_interface_instance->add_event_listener(listener_instance);

  // Normal program flow... join(), send_message(), leave()...

  gcs_control_interface_instance->join();

  gcs_control_interface_instance->leave();

  // In the end...
  gcs_control_interface_instance->remove_event_listener(ref_handler);

```
##### 内容
> 基本都是纯虚函数

- join() 与 leave()
- belongs_to_group() 返回是否已经join并属于一个组
- get_current_view() 返回一个当前视图，若已经离开组，则返回上一个视图，若未加入组，则返回空指针
- get_local_member_identifier() 返回该组此成员的本地标识符
- add_event_listener()与remove_event_listener() 注册和移除Gcs_control_event_listener
- set_xcom_cache_size(）设置XCom cache最大容量的新值



#### include/mysql/gcs/gcs_control_event_listener.h
##### 描述

- Exchanged_data内部实现为std::vector<std::pair<Gcs_member_identifier *, Gcs_message_data *>>
- Gcs_control_event_listener类，由希望接收control接口通知的用户实现。当前用于通知视图变更，传递底层的已安装的视图。
##### 内容

- Gcs_control_event_listener类
  - on_view_changed()
  - get_exchangeable_data()
  - on_suspicions() 交流基础设施中怀疑的集合变更时被调用，参数一是当前视图的成员列表，参数二是当前视图中不可达的节点的成员列表



#### include/mysql/gcs/gcs_group_management_interface.h
##### 内容

- Gcs_group_management_interface类
  - modify_configuration：修改配置，参数是一个新节点的列表
  - get_minimum_write_concurrency() 
  - get_maximum_write_concurrency()
  - get_write_concurrency()  
  - set_write_concurrency()



#### include/mysql/gcs/gcs_statistics_interface.h
##### 内容

- Gcs_statistics_interface类
  - get_total_messages_sent() 
  - get_total_bytes_sent()
  - get_total_messages_received()
  - get_total_bytes_received() 
  - get_min_message_length()
  - get_max_message_length()
  - get_last_message_timestamp() 



#### include/mysql/gcs/gcs_message.h
##### 描述

- Gcs_message_data类是gcs系统中信息的数据容器。
- Gcs_message表示组中交换的数据。一个成员发送时，应该以一个组作为目的地。接收时，则是与组相关的所有成员都会收到。

消息由两个主要的块组成：header与payload。Gcs_message的用户可以自由的添加数据到header或payload中。
每个特定的实现可以自由的使用这两个域，但是在传递到客户层之前，应该将数据从header和payload中移除。
当一个消息构建时，里面是没有任何数据的。首先，应该使用append_*方法追加数据；然后在准备发送时调用encode()方法。
在接收端，需要调用decode()方法，以便在原始消息域中恢复header与payload。
典型的发送消息的例子：
```cpp
  Gcs_control_interface       *gcs_ctrl_interface_instance;
  Gcs_communication_interface *gcs_comm_interface_instance; // obtained
                                                            // previously

  Gcs_member_identifier *myself=
    gcs_ctrl_interface_instance->get_local_information();

  Gcs_group_identifier destination("the_group");

  Gcs_message *msg= new Gcs_message(&myself, new Gcs_message_data(0, 3));

  uchar[] some_data= {(uchar)0x12,
                      (uchar)0x24,
                      (uchar)0x00};

  msg->get_message_data()->append_to_payload(&some_data,
                                             strlen(some_data));

  gcs_comm_interface_instance->send_message(msg);
```
典型的接收消息的例子：
```cpp
class my_Gcs_communication_event_listener : Gcs_communication_event_listener
  {
    my_Gcs_communication_event_listener(my_Message_delegator *delegator)
    {
      this->delegator= delegator;
    }

    void on_message_received(Gcs_message &message)
    {
      // Do something when message arrives...
      delegator->process_gcs_message(message);
    }

    private:
      my_Message_delegator *delegator;
  }
```
##### 内容

-  Gcs_message_data类
  - 构造函数，预分配header与payload需要的空间（创建消息用于发送）
  - 构造函数，预分配指定长度的消息（用于存储远程接收到的消息）
  - 追加数据到header或payload的函数，数据必须先被编码成little endian格式
  - 释放buffer的所有权，这意味着该对象不需要负责释放内部buffer的空间，该函数常与encode函数一起用
  - encode函数，将header与payload编码进内部buffer或调用者提供的buffer。全部数据以little endian格式存储，结构如下：
```cpp
   -----------------------------------------------
   | header len | payload len | header | payload |
   -----------------------------------------------
```

  - decode一个收到的消息，将所有的域填充满。
  - 获取header、header的长度、payload、payload的长度、编码数据的长度、编码payload的长度、编码header的长度
  - 成员：
    - header buffer的指针
    - header buffer下一个空位置的指针
    - header buffer已使用的长度
    - header buffer的全长
    - [ 同上的描述payload buffer的四种变量 ]
    - 指向包含header与payload的buffer的起始点的指针
    - header、payload和全部数据的buffer的长度
    - 标志当前对象是否拥有buffer并且必须在删除时释放空间
  - 废除复制与赋值构造函数
- Gcs_message类
  - 两种构造函数，注释中标明使用场景似乎有所不同
  - 获取消息的发送者、目的地、填充的消息数据
  - 成员：
    - 发送者（成员id）
    - 目的地（组id）
    - 消息数据
  - 废除复制构造函数与赋值号
#### include/mysql/gcs/gcs_member_identifier.h
##### 描述
Gcs_member_identifier类表示特定组的成员的标识符，在不同的组中可以有相同的值。
内部实现是一个字符串类型的成员id。
重载了<和=，为了可以在map和set容器中使用。
#### include/mysql/gcs/gcs_group_identifier.h
##### 描述

- Gcs_group_identifier类（内部成员是字符串类型的group_id）
  - group_id的set、get方法
  - 重载的<，使得该对象可用于map
  - 重载的=，使得该对象可用于set
#### include/mysql/gcs/gcs_types.h

- 一些需要使用的基本类型的封装
- Gcs_interface_parameters：内部实现为map的参数列表封装
#### include/mysql/gcs/gcs_view.h
##### 描述
Gcs_view类代表一个成员从属于一个组的成员关系视图。
这个对象包含：

- 属于这个视图的成员列表
- 一个Gcs_view_identifier对象
- 上一个视图中离开的成员。这应该包含离开组的本地成员
- 加入的成员，意味着它们不会出现在前一个视图。这包含加入组的本地成员，这意味着成员列表里的某一成员可以是新加入的
- 当前视图所属组的Gcs_group_identifier 

获得该对象的两个途径：

1. Gcs_control_interface::get_current_view
1. Gcs_control_event_listener::on_view_changed

禁止使用赋值运算符
#### include/mysql/gcs/gcs_view_identifier.h
##### 描述
Gcs_view_identifier类表示某一时刻的特定的视图的标识符。
这个标识符必须单调增加并且唯一。
get_representation()方法是虚函数，每个实例的实现提供自己的关于view_identifier的概念，唯一强制要求的属性是必须具有可比性。
重载了<与=，依赖于虚函数equals和lessthan，由子类自行实现内部逻辑。
其余的虚函数：clone()


#### include/mysql/gcs/gcs_logging.h
##### 内容

- Common_interface类：定义了sink、logger与debugger的通用接口
  - 析构函数
  - initialize() 初始化
  - finalize() 销毁资源
- Sink_interface类：继承Common_interface类
  - 典型用法：
```cpp
    Sink_interface *sink= new MyGCS_Sink();

    Logger_interface *logger= new MyGCS_Logger(sink);
    group_if->set_logger(logger);
```

  - log_event 有效的日志记录一些信息
  - get_information() 返回一些sink的信息
- gcs_log_level_t 日志级别的枚举量和字符串
- Logger_interface类：继承Common_interface类，用于logger系统的sink
  - 典型用法：
```cpp
    Logger_interface *logger= new MyGCS_Logger();
    group_if->set_logger(logger);
```




